#!/bin/sh

pushd ~/Code/nixos-config
home-manager switch -v --flake .#$(whoami)@$HOSTNAME --show-trace
popd
