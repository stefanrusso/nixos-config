{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    inputs.nixos-hardware.nixosModules.common-cpu-amd
    inputs.nixos-hardware.nixosModules.common-gpu-amd
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.nixos-hardware.nixosModules.common-pc-hdd
    inputs.nixos-hardware.nixosModules.common-pc
    ./hardware.nix

    ../base.nix

    ../../modules/pipewire.nix

    ../../modules/xorg.nix
    ../../modules/tuigreet.nix
    ../../modules/i3.nix

    ../../modules/printing.nix

    ../../modules/yubikey.nix

    ../../modules/steam.nix

  ];


  ###################
  ## Grub
  ###################
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    efiSupport = true;
    enableCryptodisk = true;
  };

  boot.initrd = {
    luks.devices."root" = {
      device = "/dev/disk/by-uuid/2eedcf66-4f47-4930-a2ef-fd286f99e40a";
      preLVM = true;
      keyFile = "/keyfile0.bin";
      allowDiscards = true;
    };
    secrets = { "keyfile0.bin" = "/etc/secrets/initrd/keyfile0.bin"; };
  };

  boot.supportedFilesystems = [ "ntfs" ];

  ###################
  ## Networking
  ###################
  networking.hostName = "desktop-nixos";
  networking.networkmanager.enable = true;

  ###################
  ## Locales
  ###################
  # time.timeZone = "Canada/Eastern";

  # i18n.defaultLocale = "en_CA.UTF-8";
  # i18n.supportedLocales = [ "en_CA.UTF-8/UTF-8" "en_US.UTF-8/UTF-8" "fr_CA.UTF-8/UTF-8" ];

  services.printing.enable = true;

  # users.users.stef = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" "networkmanager" "video" "scanner" "lp" ];
  # };

  environment.systemPackages = with pkgs; [
    # neovim
    # wget
    # git
    # nixpkgs-fmt
    # nixfmt
    # mlocate

    libnotify

    networkmanagerapplet
    polkit_gnome

    xdg-utils
    xdg-user-dirs

    jdk21

    gparted
  ];

  # for intellij
  environment.etc = with pkgs; {
    # "jdk".source = jdk;
    # "jdk8".source = jdk8;
    # "jdk11".source = jdk11;
    # "jdk17".source = jdk17;
    "jdk21".source = jdk21;
  };

  fonts.enableDefaultPackages = true;
  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    noto-fonts-color-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    dejavu_fonts
    font-awesome
    emojione
    siji

    fira-code-nerdfont
  ];

  environment.sessionVariables = rec {
    JAVA_HOME = "${pkgs.jdk21.home}";
  };

  xdg.portal = {
    enable = true;
    #extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  # security.polkit = {
  #   enable = true;
  #   adminIdentities = [
  #     "unix-group:wheel"
  #     "unix-user:stef"
  #   ];
  # };
  services.gnome.gnome-keyring.enable = true;
  programs.seahorse.enable = true;

  # programs.dconf.enable = true;

  # services.mullvad-vpn = {
  #   enable = true;
  #   package = pkgs.mullvad-vpn;
  # };

  # services.udisks2.enable = true;
  # services.gvfs.enable = true;

  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot

  system.stateVersion = "23.11";

}
