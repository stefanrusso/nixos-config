{ config, pkgs, libs, inputs, ... }:

{
  imports =
    [
      inputs.nixos-hardware.nixosModules.common-pc-ssd
      inputs.nixos-hardware.nixosModules.common-pc-hdd
      inputs.nixos-hardware.nixosModules.common-pc
      inputs.nixos-hardware.nixosModules.common-cpu-intel
      # inputs.nixos-hardware.nixosModules.common-gpu-nvidia
      ./hardware.nix
      ../base.nix
      ../../modules/nextcloud.nix
    ];


  #######################
  ## Bootloader
  #######################
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;


  #######################
  ## ZFS
  #######################
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.forceImportRoot = false;
  boot.kernelPackages = pkgs.zfs.latestCompatibleLinuxPackages;
  services.zfs.trim.enable = true;
  boot.zfs.extraPools = [ "storagepool" ];


  #######################
  ## Networking
  #######################
  networking = {
    useDHCP = false;
    hostName = "server";
    hostId = "16d5231a";
    interfaces = {
      eno1 = {
        useDHCP = false;
        ipv4.addresses = [{
          address = "192.168.200.100";
          prefixLength = 24;
        }];
      };
    };
    defaultGateway = "192.168.200.1";
    nameservers = [ "192.168.200.1" ];
  };

  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = "no";
    };
  };

  #######################
  ## Docker
  #######################
  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
  };

  #######################
  ## Samba
  #######################
  services.samba = {
    enable = true;

    extraConfig = ''
      browseable = yes
      smb encrypt = required
      workgroup = WORKGROUP
      server string = smbnix
      netbios name = smbnix
      security = user 
      hosts allow = 192.168.200. 127.0.0.1 localhost
      hosts deny = 0.0.0.0/0
      fruit:metadata = stream
      fruit:model = MacSamba
      fruit:posix_rename = yes 
      fruit:veto_appledouble = no
      fruit:nfs_aces = no
      fruit:wipe_intentionally_left_blank_rfork = yes 
      fruit:delete_empty_adfiles = yes
      fruit:copyfile = yes
    '';

    shares = {
      storage = {
        path = "/mnt/storagepool";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        # "force user" = "username";
        # "force group" = "groupname";
      };
      #      timemachine = {
      #        path = "/mnt/storagepool/timemachine";
      #        "valid users" = "stef";
      #        public = "yes";
      #        writeable = "yes";
      #        "read only" = "no";
      #
      #        #"force user" = "stef";  
      #
      #        "fruit:aapl" = "yes";
      #        #"inherit permissions" = "yes";
      #        "fruit:time machine" = "yes";
      #        "vfs objects" = "acl_xattr catia fruit streams_xattr";
      #
      #        "guest ok" = "yes";
      #        # "force user" = "stef";
      #        # "force group" + "nogroup";
      #      };
      homes = {
        browseable = "no";
        "read only" = "no";
        "guest ok" = "no";
      };
    };
  };

  services.samba-wsdd = {
    enable = true;
    openFirewall = true;
  };

  # services = {
  #   netatalk = {
  #     enable = true;

  #     settings = {
  #       "stef-time-machine" = {
  #         "time machine" = "yes";
  #         path = "/home/stef/time-machine";
  #         "valid users" = "stef";
  #       };
  #     };
  #   };
  # };

  services.avahi = {
    enable = true;
    nssmdns = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      hinfo = true;
      userServices = true;
      workstation = true;
    };
    extraServiceFiles = {
      smb = ''
        <?xml version="1.0" standalone='no'?><!--*-nxml-*-->
        <!DOCTYPE service-group SYSTEM "avahi-service.dtd">
        <service-group>
          <name replace-wildcards="yes">%h</name>
          <service>
            <type>_smb._tcp</type>
            <port>445</port>
          </service>
        </service-group>
      '';
    };
  };


  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 445 139 548 ];
  networking.firewall.allowedUDPPorts = [ 137 138 ];
  networking.firewall.allowPing = true;

  system.stateVersion = "23.11"; # Did you read the comment?

}
