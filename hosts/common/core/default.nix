{
  imports = [
    ./locale.nix
    ./sops.nix
    ./zsh.nix
  ];
}