{ config, ... }:
{
  time.timeZone = "Canada/Eastern";

  i18n.defaultLocale = "en_CA.UTF-8";
  i18n.supportedLocales = [ "en_CA.UTF-8/UTF-8" "en_US.UTF-8/UTF-8" "fr_CA.UTF-8/UTF-8" ];
}
