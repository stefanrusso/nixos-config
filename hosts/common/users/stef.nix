{
  users.users.stef = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "scanner" "lp" ];
    initialPassword = "password";
    shell = pkgs.zsh;
  };

}
