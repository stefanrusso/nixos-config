# Stefan's Nixos Config Todo List

## Programs to convert config to nixos/home-manager
- Bash
- Bluetooth

## Bugs to Fix
- region selection in screenshots in i3
- set background in i3 relative to i3.nix location

## Module options enable/disable
```nix
# module1.nix
{ pkgs, lib, config, ...}: 
{
    options = {
        module1.enable = lib.mkEnableOption "enables module1";
    };

    config = lib.mkIf config.module1.enable {
        option1 = 5;
        option2 = true;
    };
}

# configuration.nix
{ pkgs, lib, ...}: 
{
    imports = [
        ./nixosModules/modulebundle.nix
    ];

    module1.enable = true;
}

# modulebundle.nix
{ pkgs, lib, ...}: 
{
    imports = [
        ./module1.nix
        ./module2.nix
        ./module3.nix
    ];

    module2.enable = lib.mkDefault true;
    module3.enable = lib.mkDefault true;
}
```

## Directory Structure Cleanup

nixos-config/
├── flake.nix
├── system/
│   ├── hosts/
│   │   ├── desktop1/
│   │   │   ├── configuration.nix
│   │   │   └── hardware-configuration.nix
│   │   ├── desktop2/
│   │   │   ├── configuration.nix
│   │   │   └── hardware-configuration.nix
│   │   ├── laptop1/
│   │   │   ├── configuration.nix
│   │   │   └── hardware-configuration.nix
│   │   ├── laptop2/
│   │   │   ├── configuration.nix
│   │   │   └── hardware-configuration.nix
│   │   ├── server1/
│   │   │   ├── configuration.nix
│   │   └── server2/
│   │       ├── configuration.nix
│   │       └── hardware-configuration.nix
│   ├── modules/
│   │   ├── services/
│   │   │   ├── ssh.nix
│   │   │   ├── networkmanager.nix
│   │   │   └── ...
│   │   ├── system/
│   │   │   ├── boot.nix
│   │   │   ├── locale.nix
│   │   │   ├── users.nix
│   │   │   └── ...
│   │   ├── packages/
│   │   │   ├── editors.nix
│   │   │   ├── tools.nix
│   │   │   └── ...
│   │   └── hardware/
│   │       ├── cpu.nix
│   │       ├── disks.nix
│   │       ├── gpu.nix
│   │       └── ...
│   ├── roles/
│   │   ├── work.nix
│   │   ├── home.nix
│   │   ├── server.nix
│   │   ├── development.nix
│   │   └── ...
├── home-manager/
│   ├── users/
│   │   ├── user1/
│   │   │   ├── desktop1.nix
│   │   │   ├── desktop2.nix
│   │   │   ├── laptop1.nix
│   │   │   ├── laptop2.nix
│   │   │   ├── server1.nix
│   │   │   └── server2.nix
│   │   └── user2/
│   │       ├── desktop1.nix
│   │       ├── desktop2.nix
│   │       ├── laptop1.nix
│   │       ├── laptop2.nix
│   │       ├── server1.nix
│   │       └── server2.nix
│   ├── modules/
│   │   ├── shells.nix
│   │   ├── editors.nix
│   │   ├── media.nix
│   │   └── ...
└── README.md
