{ config, pkgs, ... }:
{
  imports = [
    ../modules/sops.nix
  ];

  services.fwupd.enable = true;

  ###################
  ## Nix & Nixpkgs
  ###################
  nix.settings = {
    auto-optimise-store = true;
    experimental-features = [ "nix-command" "flakes" ];
  };
  nixpkgs.config.allowUnfree = true;
  programs.direnv.enable = true; # for shell.nix usage

  ###################
  ## Locales
  ###################
  time.timeZone = "Canada/Eastern";

  i18n.defaultLocale = "en_CA.UTF-8";
  i18n.supportedLocales = [ "en_CA.UTF-8/UTF-8" "en_US.UTF-8/UTF-8" "fr_CA.UTF-8/UTF-8" ];

  users.users.stef = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "scanner" "lp" ];
    initialPassword = "password";
    shell = pkgs.zsh;
  };

  programs.zsh.enable = true;

  environment.systemPackages = with pkgs; [
    neovim
    wget
    git
    nixpkgs-fmt
    nixfmt-rfc-style
    mlocate
    lxqt.lxqt-policykit
  ];

  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  programs.dconf.enable = true;

  services.udisks2.enable = true;


  services.gvfs.enable = true;
}

