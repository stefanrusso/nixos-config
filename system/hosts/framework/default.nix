{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    inputs.nixos-hardware.nixosModules.framework-12th-gen-intel
    ./hardware.nix

    ../base.nix

    ../../modules/pipewire.nix

    ../../modules/xorg.nix
    ../../modules/lightdm.nix
    ../../modules/i3.nix

    ../../modules/printing.nix

    ../../modules/yubikey.nix

  ];

  ###################
  ## Grub
  ###################
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    efiSupport = true;
    enableCryptodisk = true;
  };

  boot.initrd = {
    luks.devices."root" = {
      device = "/dev/disk/by-uuid/2dcfdd48-41d0-4a96-b065-ce2d9b13366d";
      preLVM = true;
      keyFile = "/keyfile0.bin";
      allowDiscards = true;
    };
    secrets = { "keyfile0.bin" = "/etc/secrets/initrd/keyfile0.bin"; };
  };

  ###################
  ## Networking
  ###################
  networking.hostName = "framework";
  networking.networkmanager.enable = true;


  environment.systemPackages = with pkgs; [
    libnotify

    networkmanagerapplet
    polkit_gnome

    xdg-utils
    xdg-user-dirs

    jdk21
    gparted
  ];

  # for intellij
  environment.etc = with pkgs; {
    # "jdk".source = jdk;
    # "jdk8".source = jdk8;
    # "jdk11".source = jdk11;
    # "jdk17".source = jdk17;
    "jdk21".source = jdk21;
  };

  fonts.enableDefaultPackages = true;
  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    noto-fonts-color-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    dejavu_fonts
    font-awesome
    emojione
    siji

    fira-code-nerdfont
  ];

  environment.sessionVariables = rec { JAVA_HOME = "${pkgs.jdk21.home}"; };

  xdg.portal = {
    enable = true;
    #extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  services.gnome.gnome-keyring.enable = true;
  programs.seahorse.enable = true;

  # services.mullvad-vpn = {
  #   enable = true;
  #   package = pkgs.mullvad-vpn;
  # };

  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot
  services.blueman.enable = true;

  system.stateVersion = "23.11"; # Did you read the comment?

}

