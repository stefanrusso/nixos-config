{ config, lib, pkgs, ... }:
{
  programs.ssh.startAgent = false;

  services.pcscd.enable = true;

  environment.systemPackages = with pkgs; [
    gnupg
    yubikey-personalization
    yubioath-flutter
    pcscliteWithPolkit.out  ## Needed for yubioath-flutter to work properly https://github.com/NixOS/nixpkgs/issues/280826
  ];

  environment.shellInit = ''
    gpg-connect-agent /bye
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
  '';

  services.udev.packages = with pkgs; [
    yubioath-flutter
    yubikey-personalization
  ];

}