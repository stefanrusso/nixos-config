{ config, pkgs, libs, inputs, ... }:
{
  services.nextcloud = {
    enable = true;
    configureRedis = true;
    package = pkgs.nextcloud29;
    hostName = "cloud.stefanrusso.ca";
    settings.trusted_proxies = [
      "192.168.200.100"
      "172.17.0.2"
    ];
    config.adminpassFile = "/etc/nextcloud-admin-pass";

    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps) notes;
    };
    extraAppsEnable = true;
    autoUpdateApps.enable = true;

    config.defaultPhoneRegion = "CA";
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
