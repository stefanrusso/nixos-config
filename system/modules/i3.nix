{ config, pkgs, ... }: 
{
  services.xserver.windowManager.i3 = {
    enable = true;
  };

  programs.thunar = {
    enable = true;
    plugins = with pkgs.xfce; [
      thunar-volman
      thunar-dropbox-plugin
      thunar-archive-plugin
      thunar-media-tags-plugin
    ];
  };

  xdg.portal = {
    enable = true;
    configPackages = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };
}