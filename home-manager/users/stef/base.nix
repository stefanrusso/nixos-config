{ config, pkgs, lib,  ... }:

{
  imports = [
  ];

  programs.home-manager.enable = true;
  home.username = "stef";
  home.homeDirectory = "/home/stef";

  nixpkgs.config.allowUnfree = true;
  # home.useGlobalPkgs = true;

  home.stateVersion = "22.11";
}
