{ config, pkgs, lib,  ... }:

{
  imports = [
    ./base.nix
    ../../roles/console.nix
  ];

  git.signing.enable = false;
}
