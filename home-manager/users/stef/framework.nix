{ config, pkgs, lib,  ... }:

{
  imports = [
    ./base.nix
    ../../roles/console.nix
    ../../roles/desktop.nix
  ];
}
