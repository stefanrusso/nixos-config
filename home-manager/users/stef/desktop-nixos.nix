{ config, pkgs, lib, inputs, ... }:

{
  imports = [
    ./base.nix
    ../../roles/console.nix
    ../../roles/desktop.nix
    
  ];

  nixpkgs.config.allowUnfree = true;

  # Workaround for https://github.com/nix-community/home-manager/issues/2942
  nixpkgs.config.allowUnfreePredicate = (_: true);

}
