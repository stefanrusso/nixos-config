{ config, pkgs, lib, ... }:

{
  imports = [
    ../modules/git.nix
    ../modules/emacs.nix
    # "${fetchTarball "https://github.com/msteen/nixos-vscode-server/tarball/master"}/modules/vscode-server/home.nix"
  ];

  git.enable = lib.mkDefault true;
  git.signing.enable = lib.mkDefault true;

  home.packages = with pkgs; [
    neofetch
    bottom
    eza
    fzf
    xdg-user-dirs
    maven
    gcc
  ];

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestion.enable = true;
  };

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    plugins = with pkgs.vimPlugins; [ vim-nix ];
  };

  # services.vscode-server.enable = true;
}
