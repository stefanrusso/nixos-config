{ config, pkgs, lib, ... }:

{
  imports = [
    ../modules/kitty.nix
    ../modules/i3.nix
    ../modules/logseq.nix
    ../modules/firefox.nix
  ];

  home.packages = with pkgs; [
    chromium
    thunderbird
    qutebrowser

    discord
    whatsapp-for-linux

    playerctl
    xdg-user-dirs

    vlc
    celluloid

    nextcloud-client
    cryptomator

    libreoffice-fresh
    zathura
    calibre
    texstudio
    texlive.combined.scheme-medium
    evince

    gimp
    jpegoptim

    jetbrains.idea-ultimate
    jetbrains.rust-rover
  ];

  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
      # General Dev
      sonarsource.sonarlint-vscode
      formulahendry.code-runner

      # Nix/NixOS
      jnoortheen.nix-ide
      arrterian.nix-env-selector

      # Java
      redhat.java
      vscjava.vscode-java-debug
      vscjava.vscode-java-test
      vscjava.vscode-maven
      vscjava.vscode-java-dependency

      # Spring
      vscjava.vscode-spring-initializr
      ## vscjava.vscode-spring-boot-dashboard   # Manual Install
      ## vmware.vscode-spring-boot              # Manual Install

      # Latex
      james-yu.latex-workshop
    ];
  };


}
