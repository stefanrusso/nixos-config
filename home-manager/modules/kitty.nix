{ config, ... }:
{
  programs.kitty = {
    enable = true;
    theme = "Gruvbox Dark Hard";
    settings = {
      confirm_os_window_close = 0;
      # shell_integration = "enabled";
    };
  };
}