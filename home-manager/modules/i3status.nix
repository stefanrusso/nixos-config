{ config, ... }:
{
  programs.i3status = {
    enable = true;
    general = {
      colors = true;
      interval = 5;
    };
    modules = {
      "ipv6" = {
        enable = false;
      };
      "volume master" = {
        position = 1;
        settings = {
          format = "♪ %volume";
          format_muted = "♪ muted (%volume)";
          # device = "pulse:1";
        };
      };
      "disk /" = {
        position = 2;
        settings = {
          format = "/ %avail";
        };
      };
      "battery all" = {
        position = 3;
        settings = {
          format = "%status %percentage %remaining";
        };
      };

      "ethernet _first_" = {
        enable = false;
        settings = {
          format_down = "E: down";
          format_up = "E: %ip (%speed)";
        };
      };

      "load" = {
        position = 4;
        settings = {
          format = "%1min";
        };
      };

      "memory" = {
        position = 5;
        settings = {
          format = "%used | %available";
          format_degraded = "MEMORY < %available";
          threshold_degraded = "1G";
        };
      };

      "tztime local" = {
        position = 6;
        settings = {
          format = "%Y-%m-%d %H:%M:%S";
        };
      };

      "wireless _first_" = {
        enable = false;
        settings = {
          format_down = "W: down";
          format_up = "W: (%quality at %essid) %ip";
        };
      };

    };
  };
}
