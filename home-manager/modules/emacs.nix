{ config, pkgs, ... }:
{
  programs.emacs = {
    enable = true;
    package = pkgs.emacs29-gtk3;
    extraPackages = epkgs: [
      epkgs.nix-mode
    ];

    extraConfig = ''
      ;; Hide UI
      (tool-bar-mode -1);
      (scroll-bar-mode -1);

      ;; Better default modes
      (electric-pair-mode t)
      (show-paren-mode 1)
      (setq-default indent-tabs-mode nil)
      (save-place-mode t)
      (savehist-mode t)
      (recentf-mode t)
      (global-auto-revert-mode t)

      ;; Refresh package archives (GNU Elpa)
      (unless package-archive-contents
        (package-refresh-contents))

      ;; Themes
      (use-package modus-themes
        :ensure t
        :init
        (modus-themes-load-themes)
        :config
        (modus-themes-load-vivendi))
    '';
  };

  # home.file = {
  #   ".config/emacs/init.el".source = ./dotfiles/emacs/init.el;
  # };
  # xdg.configFile.emacs.source = ./dotfiles/emacs;
}
