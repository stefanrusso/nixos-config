{ config, pkgs, ... }:
{
  services.polybar = {
    enable = true;
    package = pkgs.polybarFull.override {
      i3Support = true;
      alsaSupport = true;
      pulseSupport = true;
      mpdSupport = true;
    };

    settings =
      let
        colour_background = "#282A2E";
        colour_background-alt = "#373B41";
        colour_foreground = "#C5C8C6";
        colour_primary = "#F0C674";
        colour_secondary = "#8ABEB7";
        colour_alert = "#A54242";
        colour_disabled = "#707880";
      in
      {
        "settings" = {
          screenchange-reload = true;
          pseudo-transparency = true;
        };        

        "bar/top" = {
          tray-position = "right";
          width = "100%";
          height = "24pt";
          #radius = 6;
          #dpi = 96;

          background = colour_background;
          foreground = colour_foreground;

          line-size = "3pt";

          # border-size = "4pt";
          # border-color = "#000000";

          padding-left = 0;
          padding-right = 1;

          module-margin = 1;

          separator = "|";
          separator-foreground = colour_disabled;

          font-0 = "monospace;2";

          modules-left = "xworkspaces xwindow";
          modules-right = "filesystem pulseaudio xkeyboard memory cpu battery date tray";

          cursor-click = "pointer";
          cursor-scroll = "ns-resize";

          enable-ipc = true;

          # wm-restack = "generic";
          # wm-restack = "bspwm";
          wm-restack = "i3";

          # override-redirect = true;          
        };

        "module/systray" = {
          type = "internal/tray";
          format-margin = "8pt";
          tray-spacing = "8pt";
        };

        "module/xworkspaces" = {
          type = "internal/xworkspaces";
          label-active = "%name%";
          label-active-background = colour_background-alt;
          label-active-underline = colour_primary;
          label-active-padding = 1;
          label-occupied = "%name%";
          label-occupied-padding = 1;
          label-urgent = " %name%";
          label-urgent-background = colour_alert;
          label-urgent-padding = 1;
          label-empty = "%name%";
          label-empty-foreground = colour_disabled;
          label-empty-padding = 1;
        };

        "module/xwindow" = {
          type = "internal/xwindow";
          label = "%title:0:60:...%";
        };

        "module/filesystem" = {
          type = "internal/fs";
          interval = 25;
          mount-0 = "/";
          label-mounted = "%{F#F0C674}%mountpoint%%{F-} %percentage_used%%";
          label-unmounted = "%mountpoint% not mounted";
          label-unmounted-foreground = colour_disabled;
        };

        "module/pulseaudio" = {
          type = "internal/pulseaudio";
          format-volume-prefix = "VOL ";
          format-volume-prefix-foreground = colour_primary;
          format-volume = "<label-volume>";
          label-volume = "%percentage%%";
          label-muted = "muted";
          label-muted-foreground = colour_disabled;
          click-right = "${pkgs.pavucontrol}/bin/pavucontrol &";
        };

        "module/xkeyboard" = {
          type = "internal/xkeyboard";
          blacklist-0 = "num lock";
          label-layout = "%layout%";
          label-layout-foreground = colour_primary;
          label-indicator-padding = 2;
          label-indicator-margin = 1;
          label-indicator-foreground = colour_background;
          label-indicator-background = colour_secondary;
        };

        "module/memory" = {
          type = "internal/memory";
          interval = 2;
          format-prefix = "RAM ";
          format-prefix-foreground = colour_primary;
          label = "%percentage_used:2%%";
        };

        "module/cpu" = {
          type = "internal/cpu";
          interval = 2;
          format-prefix = "CPU ";
          format-prefix-foreground = colour_primary;
          label = "%percentage:2%%";
        };

        "network-base" = {
          type = "internal/network";
          interval = 5;
          format-connected = "<label-connected>";
          format-disconnected = "<label-disconnected>";
          label-disconnected = "%{F#F0C674}%ifname%%{F#707880} disconnected";
        };

        "module/wlan" = {
          "inherit" = "network-base";
          interface-type = "wireless";
          label-connected = "%{F#F0C674}%ifname%%{F-} %essid% %local_ip%";
        };

        "module/eth" = {
          "inherit" = "network-base";
          interface-type = "wired";
          label-connected = "%{F#F0C674}%ifname%%{F-} %local_ip%";
        };

        "module/date" = {
          type = "internal/date";
          interval = 1;
          date = "%H:%M";
          date-alt = "%Y-%m-%d %H:%M:%S";
          label = "%date%";
          label-foreground = colour_primary;
        };

        "module/battery" = {
          type = "internal/battery";
          full-at = 99;
          low-at = 5;
          battery = "BAT1";
          adapter = "ADP1";
          poll-interval = 5;
          format-charging-prefix = "BAT+ ";
          format-charging-prefix-foreground = colour_primary;
          format-discharging-prefix = "BAT- ";
          format-discharging-prefix-foreground = colour_primary;
        };

      };

    script = ''
      polybar top &
    '';

  };
}
