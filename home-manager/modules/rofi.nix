{ config, pkgs, ... }:
{
  programs.rofi = {
    enable = true;
    theme = "gruvbox-dark-hard";

    extraConfig = {
      modi = "drun,run,filebrowser,window";
      show-icons = true;
      sort = true;
      matching = "fuzzy";
    };
  };
}