{ config, ... }:
{
  programs.wofi = {
    enable = true;
    settings = {
      allow_images = true;
      insensitive = true;
      show = "drun,run";
    };
    style = ''
          window {
          margin: 5px;
          border: 5px solid #282828;
          background-color: #282828;
          border-radius: 15px;
          font-family: monospace;
      }

      #input {
          margin: 5px;
          border: 2px solid #32302f;
          background-color: #32302f;
      }

      #inner-box {
          margin: 5px;
          border: 2px solid #32302f;
          background-color: #32302f;
      }

      #outer-box {
          margin: 5px;
          border: 2px solid #282828;
          background-color: #282828;
      }

      #scroll {
          margin: 5px;
          border: 2px solid #32302f;
          background-color: #32302f;
      }

      #text {
          color:  #ebdbb2;
          margin: 5px;
      }

      #entry:selected {
          background-color: #d79921;
      }

      #entry:selected label, #entry:selected image {
          color: black;
          background-color: #d79921;
          border: 0px;
      }

    '';
  };
}
