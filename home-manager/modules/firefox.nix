{ config, pkgs, lib, ... }:
{
  programs.firefox = {
    enable = true;

    profiles.default = {
      id = 0;
      name = "Default";
      isDefault = true;
      settings = {
        "browser.search.region" = "CA";
        "experiments.activeExperiment" = false;
        "experiments.enable" = false;
        "experiments.supported" = false;
      };
      search.force = true;
      search.default = "Brave";
      search.engines = {
        "Nix Packages" = {
          urls = [
            {
              template = "https://search.nixos.org/packages";
              params = [
                { name = "type"; value = "packages"; }
                { name = "query"; value = "{searchTerms}"; }
              ];
            }
          ];
          icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
          definedAliases = [ "@np" ];
        };
        "NixOS Wiki" = {
          urls = [{ template = "https://nixos.wiki/index.php?search={searchTerms}"; }];
          iconUpdateURL = "https://nixos.wiki/favicon.png";
          updateInterval = 24 * 60 * 60 * 1000; # every day    
          definedAliases = [ "@nw" ];
        };
        "Brave" = {
          urls = [{ template = "https://search.brave.com/search?q={searchTerms}"; }];
        };
      };
    };
  };
}
