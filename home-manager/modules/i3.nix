{ config, pkgs, ... }:
{
  imports = [
    ./gtk.nix
    ./kitty.nix
    # ./polybar.nix
    ./i3status.nix
    ./rofi.nix
  ];

  home.packages = with pkgs; [
    dmenu
    i3status
    i3lock
    i3blocks
    rofi
    rofi-power-menu
    brightnessctl

    maim
    xclip
    xdotool

    xarchiver
    unzip
  ];

  xsession.windowManager.i3 = {
    enable = true;
    config = {
      terminal = "${pkgs.kitty}/bin/kitty";
      modifier = "Mod4";
      menu = "${pkgs.rofi}/bin/rofi";

      bars = [
        {
          position = "bottom";
          trayOutput = "primary";
          fonts = {
            names = [ "DejaVu Sans Mono" "FontAwesome5Free" ];
            # style = "Bold Semi-Condensed";
            size = 10.0;
          };
          mode = "dock";
          hiddenState = "hide";
          statusCommand = "${pkgs.i3status}/bin/i3status";
          command = "${pkgs.i3}/bin/i3bar";
          workspaceButtons = true;
          workspaceNumbers = true;
          colors = {
            background = "#000000";
            statusline = "#ffffff";
            separator = "#666666";
            focusedWorkspace = { background = "#4c7899"; border = "#285577"; text = "#ffffff"; };
            activeWorkspace = { background = "#333333"; border = "#5f676a"; text = "#ffffff"; };
            inactiveWorkspace = { background = "#333333"; border = "#222222"; text = "#888888"; };
            urgentWorkspace = { background = "#900000"; border = "#900000"; text = "#ffffff"; };
            bindingMode = { background = "#2f343a"; border = "#900000"; text = "#ffffff"; };
          };
        }
      ];

      fonts = {
        names = [ "DejaVu Sans Mono" "FontAwesome5Free" ];
        style = "Bold Semi-Condensed";
        size = 12.0;
      };

      gaps = {
        inner = 5;
        smartGaps = true;
      };

      floating = {
        modifier = "Mod4";
        border = 1;
        titlebar = false;
        criteria = [
          { class = "^Blueman-manager$"; }
          { class = "^Pavucontrol$"; }
        ];
      };

      window = {
        border = 1;
        hideEdgeBorders = "none";
        titlebar = false;
      };

      assigns = {
        "10" = [{ class = "^thunderbird$"; } { class = "^whatsapp-for-linux$"; }];
        "9" = [{ class = "^discord$"; }];
      };

      startup = [
        { command = "systemctl --user restart polybar"; always = true; notification = false; }
        { command = "${pkgs.networkmanagerapplet}/bin/nm-applet"; always = true; notification = false; }
        { command = "nextcloud"; always = false; notification = false; }
        { command = "xinput set-prop \"PIXA3854:00 093A:0274 Touchpad\" \"libinput Tapping Enabled\" 1"; always = false; notification = false; }
        { command = "xinput set-prop \"PIXA3854:00 093A:0274 Touchpad\" \"libinput Click Method Enabled\" 0 1"; always = false; notification = false; }
        { command = "xinput set-prop \"PIXA3854:00 093A:0274 Touchpad\" \"libinput Disable While Typing Enabled\" 1"; always = false; notification = false; }
        # { command = "${pkgs.feh}/bin/feh --bg-scale ./backgrounds/big-leaves.jpg"; always = false; notification = false; }
        { command = "${pkgs.dex}/bin/dex --autostart --environment i3"; always = false; notification = false; }
        { command = "${pkgs.xss-lock}/bin/xss-lock --transfer-sleep-lock -- ${pkgs.i3lock}/bin/i3lock --nofork -c 000000"; always = false; notification = false; }
        { command = "${pkgs.blueman}/bin/blueman-applet"; always = false; notification = false; }
        { command = "${pkgs.numlockx}/bin/numlockx"; always = false; notification = false; }
        { command = "${pkgs.autotiling}/bin/autotiling"; always = false; notification = false; }
        { command = "${pkgs.dunst}/bin/dunst"; always = false; notification = false; }
        { command = "${pkgs.thunderbird}/bin/thunderbird"; always = false; notification = false; }
        { command = "${pkgs.discord}/bin/discord"; always = false; notification = false; }
        # { command = "${pkgs.cryptomator}/bin/cryptomator"; always = false; notification = false; }
        { command = "${pkgs.whatsapp-for-linux}/bin/whatsapp-for-linux"; always = false; notification = false; }
        # { command = "${pkgs.picom}/bin/picom -b"; always = false; notification = false; }
        # { command = "${pkgs.indicator-sound-switcher}/bin/indicator-sound-switcher"; always = false; notification = false; }
      ];

      colors =
        let
          color_normal_white = "#a89984";
          color_bright_white = "#ebdbb2";
          color_normal_gray = "#222222";
          color_bright_gray = "#3c3836";
          color_bright_yellow = "#d79921";
          color_normal_black = "#282828";
          color_unused = "#ff0000";
        in
        {
          focused = {
            border = color_bright_gray;
            background = color_bright_gray;
            text = color_bright_white;
            indicator = color_bright_gray;
            childBorder = color_normal_black;
          };
          focusedInactive = {
            border = color_bright_gray;
            background = color_bright_gray;
            text = color_bright_white;
            indicator = color_bright_gray;
            childBorder = color_normal_black;
          };
          unfocused = {
            border = color_normal_gray;
            background = color_normal_gray;
            text = color_normal_white;
            indicator = color_normal_gray;
            childBorder = color_normal_black;
          };
          urgent = {
            border = color_bright_yellow;
            background = color_bright_yellow;
            text = color_normal_black;
            indicator = color_unused;
            childBorder = color_unused;
          };
          placeholder = {
            border = color_unused;
            background = color_unused;
            text = color_unused;
            indicator = color_unused;
            childBorder = color_unused;
          };
        };

      modes = {
        resize = {
          "Left" = "resize shrink width 10 px or 10 ppt";
          "h" = "resize shrink width 10 px or 10 ppt";
          "Right" = "resize grow width 10 px or 10 ppt";
          "l" = "resize grow width 10 px or 10 ppt";
          "Up" = "resize shrink height 10 px or 10 ppt";
          "k" = "resize shrink height 10 px or 10 ppt";
          "Down" = "resize grow height 10 px or 10 ppt";
          "j" = "resize grow height 10 px or 10 ppt";

          "Escape" = "mode default";
          "Return" = "mode default";
        };
      };

      keybindings =
        let
          mod = config.xsession.windowManager.i3.config.modifier;
          term = config.xsession.windowManager.i3.config.terminal;
          menu = config.xsession.windowManager.i3.config.menu;
        in
        {
          #"${mod}" = "";

          "${mod}+Return" = "exec ${term}";
          "${mod}+d" = "exec ${menu} -show drun";

          "${mod}+Shift+q" = "kill";
          "${mod}+h" = "split h";
          "${mod}+v" = "split v";
          "${mod}+f" = "fullscreen toggle";
          "${mod}+s" = "layout stacking";
          "${mod}+w" = "layout tabbed";
          "${mod}+e" = "layout toggle split";
          "${mod}+Shift+space" = "floating toggle";

          "${mod}+space" = "focus mode_toggle";
          "${mod}+a" = "focus parent";
          #"${mod}+d" = "focus child";

          "${mod}+j" = "focus left";
          "${mod}+k" = "focus down";
          "${mod}+l" = "focus up";
          "${mod}+semicolon" = "focus right";

          "${mod}+Left" = "focus left";
          "${mod}+Down" = "focus down";
          "${mod}+Up" = "focus up";
          "${mod}+Right" = "focus right";

          "${mod}+Shift+j" = "move left";
          "${mod}+Shift+k" = "move down";
          "${mod}+Shift+l" = "move up";
          "${mod}+Shift+semicolon" = "move right";

          "${mod}+Shift+Left" = "move left";
          "${mod}+Shift+Down" = "move down";
          "${mod}+Shift+Up" = "move up";
          "${mod}+Shift+Right" = "move right";

          "${mod}+1" = "workspace 1";
          "${mod}+2" = "workspace 2";
          "${mod}+3" = "workspace 3";
          "${mod}+4" = "workspace 4";
          "${mod}+5" = "workspace 5";
          "${mod}+6" = "workspace 6";
          "${mod}+7" = "workspace 7";
          "${mod}+8" = "workspace 8";
          "${mod}+9" = "workspace 9";
          "${mod}+0" = "workspace 10";

          "${mod}+Shift+1" = "move container to workspace 1";
          "${mod}+Shift+2" = "move container to workspace 2";
          "${mod}+Shift+3" = "move container to workspace 3";
          "${mod}+Shift+4" = "move container to workspace 4";
          "${mod}+Shift+5" = "move container to workspace 5";
          "${mod}+Shift+6" = "move container to workspace 6";
          "${mod}+Shift+7" = "move container to workspace 7";
          "${mod}+Shift+8" = "move container to workspace 8";
          "${mod}+Shift+9" = "move container to workspace 9";
          "${mod}+Shift+0" = "move container to workspace 10";

          #S Media Keys
          "XF86AudioRaiseVolume" = "exec ${pkgs.wireplumber}/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+";
          "XF86AudioLowerVolume" = "exec ${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-";
          "XF86AudioMute" = "exec ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
          "XF86AudioMicMute" = "exec ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
          "Shift+XF86AudioMute" = "exec ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
          "${mod}+b" = "exec ${pkgs.wireplumber}/bin/wpctl set-default 60"; ## Speakers
          "${mod}+Shift+b" = "exec ${pkgs.wireplumber}/bin/wpctl set-default 35"; ## Headphones

          "XF86AudioPlay" = "exec ${pkgs.playerctl}/bin/playrctl play-pause";
          "XF86AudioNext" = "exec ${pkgs.playerctl}/bin/playrctl next";
          "XF86AudioPrevious" = "exec ${pkgs.playerctl}/bin/playrctl previous";

          "XF86MonBrightnessDown" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%-";
          "XF86MonBrightnessUp" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%-";

          ## Screenshots
          "Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim \"/home/$USER/Pictures/screenshot-$(date \"+%Y-%m-%d-%H-%M-%S\")\".png";
          "${mod}+Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim --window $(${pkgs.xdotool}/bin/xdotool getactivewindow) \"/home/$USER/Pictures/screenshot-$(date \"+%Y-%m-%d-%H-%M-%S\")\".png";
          "Shift+Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim --select \"/home/$USER/Pictures/screenshot-$(date \"+%Y-%m-%d-%H-%M-%S\")\".png";

          "Ctrl+Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png";
          "Ctrl+${mod}+Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim --window $(${pkgs.xdotool}/bin/xdotool getactivewindow) | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png";
          "Ctrl+Shift+Print" = "exec --no-startup-id ${pkgs.maim}/bin/maim --select | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png";

          "${mod}+Shift+c" = "reload";
          "${mod}+Shift+r" = "restart";
          "${mod}+Shift+e" = "exec ${pkgs.rofi}/bin/rofi -show p -modi p:rofi-power-menu";
          "${mod}+r" = "mode resize";

        };
    };
  };
}
