{ config, pkgs, lib, ... }:
{

  options = {
    git.enable = lib.mkEnableOption "enables git";
    git.signing.enable = lib.mkEnableOption "enables git signing";
  };

  config = lib.mkIf config.git.enable {
    programs.git = {
      enable = true;
      userName = "Stefan Russo";
      userEmail = "me@stefanrusso.ca";

      signing = lib.mkIf config.git.signing.enable {
        signByDefault = true;
        key = "A15E3CA2947E2958!";
      };
      extraConfig = {
        credential.helper = "${
          pkgs.git.override { withLibsecret = true; }
        }/bin/git-credential-libsecret";
      };
    };
  };

}
