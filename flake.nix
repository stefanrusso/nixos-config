{
  description = "NixOS Config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    vscode-server.url = "github:nix-community/nixos-vscode-server";
    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager, nixos-hardware, sops-nix, ... }@inputs:
    let
      inherit (self) outputs;
      forEachSystem = nixpkgs.lib.genAttrs [ "x86_64-linux" "aarch64-linux" ];
      forEachPkgs = f: forEachSystem (sys: f nixpkgs.legacyPackages.${sys});

      pkgs = import nixpkgs {
        # inherit system;
        config.allowUnfree = true;
      };

      mkNixos = modules: nixpkgs.lib.nixosSystem {
        inherit modules;
        specialArgs = { inherit inputs outputs; };
      };
      mkHome = modules: pkgs: home-manager.lib.homeManagerConfiguration {
        inherit modules pkgs;
        extraSpecialArgs = { inherit inputs outputs; };
      };

    in {
      nixosConfigurations = {
        desktop-nixos = mkNixos [ 
          ./system/hosts/desktop-nixos
          sops-nix.nixosModules.sops
          ];
        framework = mkNixos [ 
          ./system/hosts/framework
          sops-nix.nixosModules.sops
          ];
        server = mkNixos [ 
          ./system/hosts/server
          sops-nix.nixosModules.sops
        ];
      };
      
      homeConfigurations = {
        "stef@desktop-nixos" = mkHome [ ./home-manager/users/stef/desktop-nixos.nix ] nixpkgs.legacyPackages."x86_64-linux";
        "stef@framework" = mkHome [ ./home-manager/users/stef/framework.nix ] nixpkgs.legacyPackages."x86_64-linux";
        "stef@server" = mkHome [ ./home-manager/users/stef/server.nix ] nixpkgs.legacyPackages."x86_64-linux";
      };
    };
}
